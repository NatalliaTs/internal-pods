Pod::Spec.new do |s|
    s.name = 'InneractiveAdSDK'
    s.module_name = 'InneractiveAdSDK'
    s.version = '1.2.59'
    s.summary = 'Fyber VAMP SDK v7.0.6'
    s.description = 'Fyber VAMP - Video Advertising Monetization Platform for iOS devices'
    s.homepage = 'http://developer.inner-active.com/v1.0/docs'
    s.license = { :type => 'Copyright', :text => 'Copyright 2018 Fyber, All rights reserved.' }
    s.author = { 'Fyber' => 'http://developer.inner-active.com/v1.0/docs' }
    s.source = { :http => 'https://app-craft-internal.ams3.digitaloceanspaces.com/Frameworks/InneractiveAdSDK/InneractiveAdSDK-iOS-v7.0.6.zip' }
    s.platforms = {:ios => '10.0'}
    s.requires_arc = true
    s.frameworks = [
      'SystemConfiguration',
      'CoreGraphics',
      'EventKit',
      'EventKitUI',
      'MediaPlayer',
      'MessageUI',
      'CoreTelephony',
      'StoreKit',
      'AdSupport',
      'AVFoundation',
      'CoreMedia',
      'WebKit'
    ]
    s.libraries = 'xml2.2'
    s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4' }

    s.subspec 'IASDKCore' do |core|
      core.vendored_frameworks = 'IASDKCore/IASDKCore.framework'
      core.frameworks = [
        'SystemConfiguration',
        'CoreGraphics',
        'EventKit',
        'EventKitUI',
        'MediaPlayer',
        'MessageUI',
        'CoreTelephony',
        'StoreKit',
        'AdSupport',
        'AVFoundation',
        'CoreMedia',
        'WebKit'
      ]
      core.libraries = 'xml2.2'
      core.resources = 'IASDKCore/IASDKResources.bundle'
      core.xcconfig = { :OTHER_LDFLAGS => '-ObjC'}
      core.platforms = {:ios => '10.0'}
    end

    s.subspec 'IASDKMRAID' do |mraid|
      mraid.vendored_frameworks = 'IASDKMRAID/IASDKMRAID.framework'
      mraid.platforms = {:ios => '10.0'}
    end

    s.subspec 'IASDKVideo' do |video|
      video.vendored_frameworks = 'IASDKVideo/IASDKVideo.framework'
      video.platforms = {:ios => '10.0'}
    end
  end
